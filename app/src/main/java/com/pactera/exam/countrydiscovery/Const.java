package com.pactera.exam.countrydiscovery;

/**
 * Use for global variables in the project
 */

public class Const {
    public static final String URL_JSON_FEED = "https://dl.dropboxusercontent.com/u/746330/facts.json";
    public static final String EMPTY = "";
}
