package com.pactera.exam.countrydiscovery;

import android.util.Log;

/**
 * Use this class for sending log output.
 */

public class Logger {
    private static final String TAG = "CountryDiscovery";

    /**
     * Sends a DEBUG log message.
     * @param msg  the message you would like logged.
     */
    public static void d(String msg) {
        Log.d(TAG, msg);
    }
}
