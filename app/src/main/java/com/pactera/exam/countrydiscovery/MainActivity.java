package com.pactera.exam.countrydiscovery;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.pactera.exam.countrydiscovery.needle.Needle;

import java.util.ArrayList;

/**
 * A simple activity class that downloads a JSON feed and displays its content in the ListView.
 */

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout mSwipeRefreshLayout;
    private FactList mFactList;
    private ListView mFactListView;
    private FactListAdapter mFactListAdapter;
    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mFactList = FactList.getInstance();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up the Swipe Refresh layout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2);

        // Set up the Action Bar object
        mActionBar = getSupportActionBar();
        setActionBarTitle(Const.EMPTY);

        // Set up the ListView object
        mFactListView = (ListView) findViewById(R.id.discovery_fact_list_view);
        // Define the custom adapter for ListView
        mFactListAdapter = new FactListAdapter(this, new ArrayList<Fact>());
        mFactListView.setAdapter(mFactListAdapter);

        // Download a JSON feed from the server and display its content in the ListView
        refreshFactListView(Const.URL_JSON_FEED);
    }

    /**
     * Sets a title on the Action Bar.
     *
     * @param title  the title to be displayed on Action Bar.
     */
    public void setActionBarTitle(String title) {
        mActionBar.setTitle(title);
    }

    /**
     * Shows an error message in the toast.
     *
     * @param resId  the string that specified by resId to be displayed in the toast.
     */
    public void showErrorMessage(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    /**
     * Downloads a JSON feed and displays its content in the ListView.
     *
     * @param url   the url that is to be used for download a JSON feed.
     */
    private void refreshFactListView(final String url) {
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                JsonDownloader jsonDownloader = new JsonDownloader();
                Response response = jsonDownloader.loadFeed(url);
                if (response != null) {
                    final boolean result = mFactList.store(response.title, response.facts);

                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Logger.d("Reloading the fact list");

                            ArrayList<Fact> facts = (result == true) ? new ArrayList(mFactList.getFacts()) : new ArrayList<Fact>();
                            // Update and notify the fact list adapter about received data
                            mFactListAdapter.updateItemList(facts);
                            mFactListAdapter.notifyDataSetChanged();
                            // Set a title (come from the JSON) in the Action Bar
                            setActionBarTitle(mFactList.getTitle());
                        }
                    });
                } else {
                    showErrorMessage(R.string.error_download_failed);
                }
            }
        });
    }

    /**
     * Handles a refresh action that sent from the SwipeRefreshLayout.
     */
    @Override
    public void onRefresh() {
        Logger.d("onRefresh: Refreshing a content of ListView...");

        // Get latest JSON feed from the server and display its content into ListView
        refreshFactListView(Const.URL_JSON_FEED);

        mSwipeRefreshLayout.setRefreshing(false);
    }
}
