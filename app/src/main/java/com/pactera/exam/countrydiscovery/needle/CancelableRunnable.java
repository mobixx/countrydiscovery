package com.pactera.exam.countrydiscovery.needle;

public interface CancelableRunnable extends Runnable {

	void cancel();

	boolean isCanceled();
}
