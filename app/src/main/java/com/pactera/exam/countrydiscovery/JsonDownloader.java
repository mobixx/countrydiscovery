package com.pactera.exam.countrydiscovery;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.io.Reader;

/**
 * Use this class for downloading JSON feed from the server.
 */

public class JsonDownloader {
    final private int READ_TMO = 10000; /* milliseconds */
    final private int CONN_TMO = 15000; /* milliseconds */
    final private int HTTP_OK = 200; // response code
    final private String EOL = "\n";


    /**
     * Load a JSON feed from a given URL and parse it uses Gson library.
     *
     * @param address  the url that is to be used for download a JSON feed.
     * @return         the response that will be then used for populates a {@link FactList} object.
     */
    public Response loadFeed(String address) {
        Response response = null;
        InputStream is = null;

        try {
            URL url = new URL(address);
            // set up the http connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TMO);
            conn.setConnectTimeout(CONN_TMO);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // start the query
            conn.connect();
            int responseCode = conn.getResponseCode();
            if (responseCode == HTTP_OK) {
                Gson gson = new Gson();

                is = conn.getInputStream();
                Reader reader = new InputStreamReader(is);
                // parse the received json
                response = gson.fromJson(reader, Response.class);
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return response;
    }
}
