package com.pactera.exam.countrydiscovery;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that holds the title and facts which parsed from the JSON feed. This class implemented
 * as singleton in order to share a data between threads.
 */

public class FactList {
    private final String LOG_TAG = FactList.class.getSimpleName() + ": ";
    private List<Fact> mFacts = null;
    private String mTitle = null;
    private static FactList sInstance = new FactList();

    /**
     * Don't instantiate directly - use {@link #getInstance()} instead.
     */
    private FactList() {}

    /**
     * Returns a static {@link FactList} object.
     *
     * @return   the result that holds a static {@link FactList} object.
     */
    public static FactList getInstance() {
        if (sInstance == null) {
            synchronized (FactList.class) { // for multi-threading
                if(sInstance == null){
                    sInstance = new FactList();
                }
            }
        }
        return sInstance;
    }

    /**
     * Stores the facts array and its title used to display in the ListView.
     *
     * @param title  the title to be displayed on the Action Bar
     * @param facts  the facts to be populated in the ListView
     * @return       whether or not that this is allowed to be stored.
     */
    public boolean store(String title, List<Fact> facts) {
        if (title != null && facts != null) {
            mTitle = title;

            mFacts = new ArrayList<Fact>();
            for (Fact fact : facts) {
                if (fact.title == null && fact.description == null && fact.imageUrl == null) {
                    Logger.d(LOG_TAG + "a nullable object from list deleted.");
                } else {
                    mFacts.add(fact);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Checks to see whether or not a fact array initialized.
     *
     * @return  whether or not that this is initialized.
     */
    public boolean isInitialized() {
        return (mFacts != null);
    }

    /**
     * Returns an array of {@link Fact} objects that stored into the object.
     *
     * @return  the result that holds array of {@link Fact} objects.
     */
    public List<Fact> getFacts() {
        if (isInitialized()) {
            return mFacts;
        }
        return null;
    }

    /**
     * Returns a {@link Fact} object that specified by index.
     *
     * @param index   the index of a specific fact in the array.
     * @return        the result that holds a {@link Fact} object.
     */
    public Fact getFact(int index) {
        Fact fact = null;

        if (index >= 0 && index < mFacts.size()) {
            fact = mFacts.get(index);
        }
        return fact;
    }

    /**
     * Returns a title that defined for those facts.
     *
     * @return   the string that holds a title.
     */
    public String getTitle() {
        return mTitle;
    }
}
