package com.pactera.exam.countrydiscovery;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Use for storing the feed contents which parse from JSON feed by gson library.
 */

public class Response {
    @SerializedName("title")
    public String title;

    @SerializedName("rows")
    public List<Fact> facts;
}