package com.pactera.exam.countrydiscovery;

import com.google.gson.annotations.SerializedName;

/**
 * A class that holds fact object which parsed from the JSON feed.
 */

public class Fact {
    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("imageHref")
    public String imageUrl;
}