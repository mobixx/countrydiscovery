package com.pactera.exam.countrydiscovery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A class that creates, holds, and adapts the fact data for a ListView object.
 */

public class FactListAdapter extends BaseAdapter {
    private final String LOG_TAG = FactListAdapter.class.getSimpleName() + ": ";
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Fact> mDataSource;

    public FactListAdapter(Context context, ArrayList<Fact> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Updates the facts array which used to display in the ListView.
     *
     * @param items   the items to be populated in the ListView.
     */
    public void updateItemList(ArrayList<Fact> items) {
        mDataSource = items;
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get view for row item
        View rowView = mInflater.inflate(R.layout.list_item_fact, parent, false);

        // Get title element
        TextView titleTextView = (TextView) rowView.findViewById(R.id.fact_list_title);

        // Get description element
        TextView descriptionTextView = (TextView) rowView.findViewById(R.id.fact_list_description);

        // Get thumbnail element
        ImageView thumbnailImageView = (ImageView) rowView.findViewById(R.id.fact_list_thumbnail);

        // Populate each element with relevant data

        // Get a object (i.e fact)
        Fact fact = (Fact) getItem(position);

        // Set the title and description of fact
        titleTextView.setText(fact.title);
        if (fact.description == null) {
            Logger.d(LOG_TAG + "a description field in item " + position + " not defined");
        } else {
            // Delete a whitespace in the begin and end
            descriptionTextView.setText(fact.description.trim());
        }

        // Download an image asynchronously (through Picasso library)
        if (fact.imageUrl == null) {
            Logger.d(LOG_TAG + "Skipping a download image in item " + position + " because an imageUrl field not defined");
        } else {
            // If an image is not available then displays a default placeholder.
            Picasso.with(mContext).load(fact.imageUrl).placeholder(R.mipmap.ic_launcher).into(thumbnailImageView);
        }

        return rowView;
    }
}
